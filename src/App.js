import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect, Route, Switch } from "react-router-dom";

import routepath from "constant/routepath";
import Loader from "modules/components/shared/Loader";
import Layout from "modules/components/shared/Layout";
import { authenticate } from "actions/auth";

const Home = React.lazy(() => import("modules/components/Home"));
const Dish = React.lazy(() => import("modules/components/Dish"));
const Login = React.lazy(() => import("modules/components/Login"));
const DishRank = React.lazy(() => import("modules/components/DishRank"));
const NotFound = React.lazy(() => import("modules/components/NotFound"));

const RouteWrapper = ({ user, ...props }) => {
  if (!user) {
    return <Redirect to={routepath.LOGIN} />;
  }

  return <Route {...props} />;
};

const App = () => {
  const dispatch = useDispatch();
  const { isLoading, user } = useSelector((state) => state.user);

  useEffect(() => {
    dispatch(authenticate());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isLoading) {
    return <Loader />;
  }

  return (
    <React.Suspense fallback={<Loader />}>
      <Switch>
        <Route exact path={routepath.LOGIN} component={Login} />
        <Layout>
          <Switch>
            <RouteWrapper
              exact
              user={user}
              path={routepath.HOME}
              component={Home}
            />
            <RouteWrapper
              exact
              user={user}
              path={routepath.DISH_RANK}
              component={DishRank}
            />
            <RouteWrapper
              exact
              user={user}
              path={routepath.DISH}
              component={Dish}
            />
            <Route component={NotFound} />
          </Switch>
        </Layout>
      </Switch>
    </React.Suspense>
  );
};

export default App;
