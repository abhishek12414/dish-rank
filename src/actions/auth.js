import { createAction } from "@reduxjs/toolkit";
import routepath from "constant/routepath";
import { getFromStorage, saveToStorage } from "utils/storage";
import { getUsers } from "../apis/user";
import { redirect } from "./navigation";

export const setUser = createAction("SET_USER");
export const setIsLoading = createAction("SET_IS_USER_LOADING");
export const setErrorMsg = createAction("SET_ERROR_MSG");

export const login = (props) => (dispatch) => {
  dispatch(setIsLoading(true));
  return getUsers()
    .then(({ data }) => {
      const userObj = data.find(
        (user) =>
          user.username === props.username && user.password === props.password
      );
      if (userObj) {
        dispatch(setUser(userObj));
        dispatch(setErrorMsg(""));
        saveToStorage("user", JSON.stringify(userObj));
      } else {
        dispatch(setErrorMsg("Please Enter valid credentials"));
      }
    })
    .catch(() => dispatch(setUser(null)))
    .finally(() => dispatch(setIsLoading(false)));
};

export const authenticate = () => (dispatch) => {
  dispatch(setIsLoading(true));
  return getFromStorage("user")
    .then((user) => {
      const storedUser = JSON.parse(user);
      if (!storedUser) {
        dispatch(redirect(routepath.LOGIN));
        dispatch(setIsLoading(false));
        return;
      }

      getUsers()
        .then(({ data }) => {
          const userObj = data.find(
            (user) =>
              user.username === storedUser.username &&
              user.password === storedUser.password
          );

          if (userObj) {
            dispatch(setUser(userObj));
            dispatch(setErrorMsg(""));
          } else {
            dispatch(redirect(routepath.LOGIN));
          }
        })
        .finally(() => dispatch(setIsLoading(false)));
    })
    .catch(() => dispatch(setUser(null)));
};

export const logout = () => (dispatch) => {
  return saveToStorage("user", null).then(() => {
    dispatch(setUser(null));
  });
};
