import { createAction } from "@reduxjs/toolkit";

import {
  updateDishRank,
  getDishRatingAndScore,
  getUserSelectedDishes,
} from "utils/dishRankComputation";
import { getDishes } from "../apis/dish";

export const setDishes = createAction("SET_DISHES");
export const setIsLoading = createAction("SET_FETCH_DISHES");
export const setDishesScore = createAction("SET_DISHES_SCORE");
export const setUserSelectedDishes = createAction("SET_USER_SELECTED_DISHES");

export const getAllDishes = () => (dispatch) => {
  dispatch(setIsLoading());
  return getDishes()
    .then(({ data }) => dispatch(setDishes(data)))
    .catch(() => dispatch(setDishes([])))
    .finally(() => dispatch(setIsLoading()));
};

export const getAllDishesScore = () => (dispatch) => {
  dispatch(setIsLoading());
  return getDishes()
    .then(({ data }) => {
      getDishRatingAndScore(data).then((res) => dispatch(setDishesScore(res)));
    })
    .catch(() => dispatch(setDishesScore([])))
    .finally(() => dispatch(setIsLoading()));
};

export const postUserDishChoice = (props) => (dispatch, getState) => {
  const { user } = getState();
  return updateDishRank(props, user.user.id);
};

export const getUserDishes = () => (dispatch, getState) => {
  const { user } = getState();
  return getUserSelectedDishes(user.user.id)
    .then((res) => dispatch(setUserSelectedDishes(res)))
    .catch(() => dispatch(setUserSelectedDishes([])));
};
