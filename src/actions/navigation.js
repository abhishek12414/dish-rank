import { push } from "connected-react-router";

export const redirect = (route) => (dispatch) => {
  dispatch(push({ pathname: route }));
};
