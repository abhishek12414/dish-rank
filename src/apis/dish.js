import axios from "axios";

export const getDishes = () =>
  axios.get(
    "https://raw.githubusercontent.com/syook/react-dishpoll/main/db.json"
  );
