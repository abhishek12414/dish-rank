import axios from "axios";

export const getUsers = () =>
  axios.get(
    "https://raw.githubusercontent.com/syook/react-dishpoll/main/users.json"
  );
