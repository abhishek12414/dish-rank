export default Object.freeze({
  HOME: "/",
  LOGIN: "/login",
  DISH: "/dish",
  DISH_RANK: "/dish/rank",
});
