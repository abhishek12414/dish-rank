import React from "react";
import cx from "classnames";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import SelectBackDrop from "./SelectBackDrop";

const useStyles = makeStyles({
  root: {
    width: 300,
    position: "relative",
    cursor: "pointer",
  },
  media: {
    height: 300,
    width: 300,
  },
});

const DishCard = ({ dish, className, choiceRank, onClick, ...props }) => {
  const classes = useStyles();

  return (
    <Card
      className={cx(classes.root, className)}
      onClick={() => onClick(dish.id)}
      {...props}
    >
      {!!choiceRank && <SelectBackDrop choiceRank={choiceRank} />}
      <CardMedia
        className={classes.media}
        image={dish.image}
        title={dish.name}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="h2">
          {dish.dishName}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {dish.description}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default DishCard;
