import React from "react";
import Skeleton from "@material-ui/lab/Skeleton";
import Box from "@material-ui/core/Box";

const DishCardSkeleton = ({ ...props }) => {
  return (
    <div style={{ width: 300 }} {...props}>
      <Skeleton variant="rect" width={300} height={300} />
      <Box>
        <Skeleton width="60%" />
        <Skeleton />
      </Box>
    </div>
  );
};

export default DishCardSkeleton;
