import React from "react";
import cx from "classnames";

import { green } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";
import CheckCircle from "@material-ui/icons/CheckCircle";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  root: {
    width: 300,
    height: "100%",
    position: "absolute",
    zIndex: 10,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  backDrop: {
    filter: "opacity(0.4)",
    background: green[500],
    width: 300,
    height: "100%",
    position: "absolute",
    zIndex: "-1",
  },
  selectedRank: {
    fontSize: "2rem",
    color: "#fff",
  },
  checkIcon: {
    fill: "#fff",
  },
});
const SelectBackDrop = ({ choiceRank }) => {
  const classes = useStyles();

  return (
    <div className={cx(classes.root)}>
      <div className={cx(classes.backDrop)} />
      <div style={{ textAlign: "center" }}>
        <CheckCircle className={classes.checkIcon} fontSize="large" />
        <Typography component="h3" className={classes.selectedRank}>
          Choice {choiceRank}
        </Typography>
      </div>
    </div>
  );
};

export default SelectBackDrop;
