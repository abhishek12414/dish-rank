import React, { useEffect, useState } from "react";
import Alert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";

import { getAllDishes, postUserDishChoice } from "actions/dish";

import DishCard from "./components/DishCard";
import Empty from "modules/components/shared/Empty";
import DishCardSkeleton from "./components/DishCardSkeleton";
import ConfirmationModal from "../shared/ConfirmationModal";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    [theme.breakpoints.down("xs")]: {
      justifyContent: "center",
    },
  },
  card: {
    margin: theme.spacing(2),
  },
}));

const Dish = () => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const { isLoading, dishes } = useSelector((state) => state.dish);
  const [userChoice, setUserChoice] = useState([]);
  const [open, setOpen] = useState(false);
  const [displayAlert, setDisplayAlert] = useState(false);

  useEffect(() => {
    dispatch(getAllDishes());
    // eslint-disable-next-line
  }, []);

  const onCardClick = (id) => {
    if (userChoice.includes(id)) {
      let choices = [...userChoice];
      choices.splice(choices.indexOf(id), 1);

      setUserChoice(choices);
    } else {
      if (userChoice.length !== 3) {
        setUserChoice((p) => [...p, id]);
      }
    }
  };

  useEffect(() => {
    userChoice.length === 3 && setOpen(true);
  }, [userChoice]);

  const handleClose = async (props) => {
    if (props) {
      await dispatch(postUserDishChoice(userChoice));
    }
    setOpen(false);
    setDisplayAlert(true);
  };

  if (isLoading || !dishes) {
    return (
      <div className={classes.root}>
        {new Array(8).fill(1).map((_, i) => (
          <DishCardSkeleton key={i} className={classes.card} />
        ))}
      </div>
    );
  }

  if (!dishes || dishes?.length < 1) {
    return <Empty />;
  }

  return (
    <div className={classes.root}>
      {dishes.map((dish, index) => (
        <DishCard
          dish={dish}
          key={index}
          className={classes.card}
          onClick={onCardClick}
          choiceRank={userChoice.indexOf(dish.id) + 1}
        />
      ))}
      <ConfirmationModal
        title="Information"
        description="Do you want to submit your choice"
        handleClose={handleClose}
        open={open}
      />
      <Snackbar
        open={displayAlert}
        autoHideDuration={6000}
        onClose={() => setDisplayAlert(false)}
      >
        <Alert onClose={() => setDisplayAlert(false)} severity="success">
          Record saved successfully.
        </Alert>
      </Snackbar>
    </div>
  );
};

export default Dish;
