import React from "react";
import cx from "classnames";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import DishRating from "./DishRating";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: 800,
    marginBottom: theme.spacing(2),
  },
  cardContent: {
    display: "flex",
    flex: 1,
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
    },
  },
  imageContainer: {
    [theme.breakpoints.down("xs")]: {
      display: "flex",
      alignItems: "center",
    },
  },
  media: {
    height: 100,
    width: 100,
  },
  content: {
    flex: 1,
    padding: theme.spacing(1),
    [theme.breakpoints.down("xs")]: {
      padding: `${theme.spacing(1)}px 0`,
    },
  },
  ratingContainer: {
    flex: 0.2,
  },
  isSelected: {
    backgroundColor: "#b2dfdb",
  },
}));

const DishRankCard = ({
  dish,
  maxScore,
  className,
  choiceRank,
  onClick,
  isSelected,
  ...props
}) => {
  const classes = useStyles();

  return (
    <Card
      className={cx(
        classes.root,
        className,
        isSelected ? classes.isSelected : ""
      )}
      {...props}
    >
      <CardContent className={classes.cardContent}>
        <div className={classes.imageContainer}>
          <CardMedia
            className={classes.media}
            image={dish.image}
            title={dish.name}
          />
        </div>
        <div className={classes.content}>
          <Typography gutterBottom variant="h5" component="h2">
            {dish.dishName}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {dish.description}
          </Typography>
        </div>
        <DishRating
          className={classes.ratingContainer}
          dish={dish}
          maxScore={maxScore}
        />
      </CardContent>
    </Card>
  );
};

export default DishRankCard;
