import React from "react";
import cx from "classnames";
import Box from "@material-ui/core/Box";
import Skeleton from "@material-ui/lab/Skeleton";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    width: 800,
    marginBottom: theme.spacing(2),
  },
  box: {
    flex: 1,
    marginLeft: theme.spacing(2),
  },
  card: {
    margin: theme.spacing(2),
  },
  ratingContainer: {
    flex: 0.2,
    marginLeft: theme.spacing(2),
  },
}));

const DishRankSkeleton = ({ className, ...props }) => {
  const classes = useStyles();

  return (
    <Box className={cx(classes.root, className)} {...props}>
      <div>
        <Skeleton variant="rect" width={100} height={100} />
      </div>
      <Box className={classes.box}>
        <Skeleton width={"30%"} />
        <Skeleton width={"50%"} />
        <Skeleton width={"70%"} />
        <Skeleton width={"80%"} />
        <Skeleton width={"100%"} />
      </Box>
      <Box className={classes.ratingContainer}>
        <Skeleton width={"40%"} />
        <Skeleton width={"80%"} />
        <Skeleton width={"80%"} />
        <Skeleton width={"80%"} />
      </Box>
    </Box>
  );
};

export default DishRankSkeleton;
