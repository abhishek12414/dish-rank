import React from "react";
import cx from "classnames";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import LinearProgress from "@material-ui/core/LinearProgress";
import Box from "@material-ui/core/Box";
import Star from "@material-ui/icons/Star";
import { yellow, green, orange } from "@material-ui/core/colors";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
  },
  card: {
    margin: theme.spacing(2),
  },
  ratingRow: {
    display: "flex",
    alignItems: "center",
  },
  icon: {
    fontSize: 18,
  },
  progressbar: {
    width: "100%",
    margin: "0 4px",
    borderRadius: 8,
  },
  progressColor1: {
    "& > div": {
      backgroundColor: orange[500],
    },
  },
  progressColor2: {
    "& > div": {
      backgroundColor: yellow[500],
    },
  },
  progressColor3: {
    "& > div": {
      backgroundColor: green[500],
    },
  },
}));

const DishRating = ({ dish, className, maxScore }) => {
  const classes = useStyles();

  const maxRating = Math.max(...Object.values(dish?.rating ?? [1]));

  return (
    <div className={cx(className)}>
      <Typography>Score {dish.score ?? 0}</Typography>
      {[3, 2, 1].map((key) => {
        return (
          <Box key={key} className={classes.ratingRow}>
            <Typography variant="body1">{key}</Typography>
            <Star className={classes.icon} />
            <LinearProgress
              className={cx(
                classes.progressbar,
                classes[`progressColor${key}`]
              )}
              variant="determinate"
              value={((dish?.rating?.[key] ?? 0) / maxRating) * 100}
            />
            {dish?.rating?.[key] ?? 0}
          </Box>
        );
      })}
    </div>
  );
};

export default React.memo(DishRating);
