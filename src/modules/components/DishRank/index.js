import React, { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core/styles";

import { getAllDishesScore, getUserDishes } from "actions/dish";

import DishRankCard from "./components/DishRankCard";
import DishRankSkeleton from "./components/DishRankSkeleton";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
  },
  card: {
    margin: theme.spacing(2),
  },
}));

const DishRank = () => {
  const classes = useStyles();

  const dispatch = useDispatch();
  const { isLoading, dishesScore, userSelectedDishes } = useSelector(
    (state) => state.dish
  );
  const maxScore = useMemo(() => dishesScore?.[0]?.score ?? 0, [dishesScore]);

  useEffect(() => {
    dispatch(getAllDishesScore());
    dispatch(getUserDishes());
    // eslint-disable-next-line
  }, []);

  if (isLoading || !dishesScore) {
    return (
      <div className={classes.root}>
        {new Array(8).fill(1).map((_, i) => (
          <DishRankSkeleton key={i} className={classes.card} />
        ))}
      </div>
    );
  }

  return (
    <div className={classes.root}>
      {dishesScore.map((dish, index) => (
        <DishRankCard
          dish={dish}
          key={index}
          className={classes.card}
          maxScore={maxScore}
          isSelected={userSelectedDishes.includes(dish.id)}
        />
      ))}
    </div>
  );
};

export default DishRank;
