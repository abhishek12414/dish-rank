import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flex: 1,
    padding: theme.spacing(2),
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    [theme.breakpoints.down("xs")]: {
      flex: 1,
      display: "flex",
      alignItems: "center",
      flexDirection: "column",
    },
  },
  logo: {
    height: theme.spacing(16),
  },
  typography: {
    [theme.breakpoints.down("xs")]: {
      fontSize: 42,
    },
  },
}));

const Home = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img
        src={
          "https://cdn.iconscout.com/icon/free/png-256/fast-food-1851561-1569286.png"
        }
        alt="Classroom"
        className={classes.logo}
      />
      <Typography variant="h1" className={classes.typography}>
        My Dish Rank
      </Typography>
    </div>
  );
};

export default Home;
