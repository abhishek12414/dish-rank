import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles((theme) => ({
  left: {
    flex: 1,
    padding: theme.spacing(2),
    alignItems: "center",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    [theme.breakpoints.down("xs")]: {
      flex: 1,
      display: "flex",
      alignItems: "center",
      flexDirection: "column",
    },
  },
  logo: {
    height: theme.spacing(16),
  },
}));

const SideLogo = () => {
  const styles = useStyles();

  return (
    <div className={styles.left}>
      <img
        src={
          "https://cdn.iconscout.com/icon/free/png-256/fast-food-1851561-1569286.png"
        }
        alt="Classroom"
        className={styles.logo}
      />
      <Typography variant="h6">Rank Your Food</Typography>
    </div>
  );
};

export default SideLogo;
