import React, { useState, useEffect } from "react";
import { Redirect } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import TextField from "@material-ui/core/TextField";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import SideLogo from "./SideLogo";
import { login } from "actions/auth";
import routePaths from "constant/routepath";
import Button from "modules/widgets/Button";

const useStyles = makeStyles((theme) => ({
  container: {
    display: "flex",
    width: 600,
    height: 300,
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    margin: "auto",
    borderRadius: 8,
    overflow: "hidden",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
      width: "auto",
      height: "100%",
      margin: 0,
    },
  },
  right: {
    flex: 2.5,
    background: "#fff",
    padding: theme.spacing(2),
  },
  leftContainer: {
    display: "flex",
    flexDirection: "row-reverse",
    marginTop: theme.spacing(1),
  },
  form: {
    display: "flex",
    flexDirection: "column",
    "& > *": {
      margin: `${theme.spacing(1)}px 0`,
    },
  },
  button: {
    marginTop: theme.spacing(3),
  },
}));

const Login = () => {
  const dispatch = useDispatch();
  const { isLoading, errorMsg, user } = useSelector((state) => state.user);
  const classes = useStyles();

  const [userDetail, setUserDetail] = useState({
    username: "",
    password: "",
  });
  const [showError, setShowError] = useState(false);

  const [errors, setErrors] = useState({ username: "", password: "" });

  useEffect(() => {
    if (showError) {
      validateField();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userDetail, showError]);

  if (user) {
    return <Redirect to={routePaths.HOME} />;
  }

  const onChange = (e) => {
    setUserDetail({
      ...userDetail,
      [e.target.name]: e.target.value,
    });
    showError ? validateField() : setShowError(true);
  };

  const validateField = () => {
    const newErrors = { ...errors };
    let isValid = true;
    Object.entries(userDetail).forEach(([key, value]) => {
      if (value === "") {
        isValid = false;
        newErrors[key] = "This filed is required";
      } else {
        newErrors[key] = "";
      }
    });

    setErrors(newErrors);
    return isValid;
  };

  const handleSubmit = () => {
    const isValidForm = validateField();
    if (!isValidForm) {
      return;
    }

    dispatch(login(userDetail));
  };

  return (
    <div className={classes.container}>
      <SideLogo />
      <div className={classes.right}>
        <Typography variant="h6">SignIn</Typography>
        <div className={classes.form}>
          <TextField
            label="Username"
            name="username"
            onChange={onChange}
            error={!!errors?.username}
            helperText={errors?.username ?? ""}
          />
          <TextField
            type="password"
            label="Password"
            name="password"
            onChange={onChange}
            error={!!errors?.password}
            helperText={errors?.password ?? ""}
          />
          <Button
            variant="contained"
            type="submit"
            color="primary"
            onClick={handleSubmit}
            loading={isLoading}
            className={classes.button}
          >
            Submit
          </Button>
          {errorMsg && <Typography>{errorMsg}</Typography>}
        </div>
      </div>
    </div>
  );
};

export default Login;
