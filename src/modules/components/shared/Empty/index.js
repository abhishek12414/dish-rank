import React from "react";
import HourglassEmpty from "@material-ui/icons/HourglassEmpty";
import { makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: "80vh",
  },
});

const Empty = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <HourglassEmpty />
      <Typography component="p">No Record Found</Typography>
    </div>
  );
};

export default Empty;
