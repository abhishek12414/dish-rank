import React from "react";
import cx from "classnames";
import { Link, useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";

import routepath from "constant/routepath";
import { logout } from "actions/auth";
import { redirect } from "actions/navigation";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
  },
  logo: {
    width: 48,
    height: 48,
  },
  typography: {
    textDecoration: "none",
    marginRight: theme.spacing(2),
  },
  active: {
    borderBottom: "1px solid",
  },
}));

const Header = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { isLoading, user } = useSelector((state) => state.user);
  const {
    location: { pathname },
  } = useHistory();

  const redirectTo = (path) => dispatch(redirect(path));

  if (isLoading) {
    return null;
  }

  const loginButton = (
    <Button color="inherit" onClick={() => redirectTo(routepath.LOGIN)}>
      Login
    </Button>
  );

  const authMenu = (
    <div>
      <Typography
        color="inherit"
        className={cx(
          classes.typography,
          pathname === routepath.DISH ? classes.active : ""
        )}
        component={Link}
        to={routepath.DISH}
      >
        Dish List
      </Typography>
      <Typography
        color="inherit"
        className={cx(
          classes.typography,
          pathname === routepath.DISH_RANK ? classes.active : ""
        )}
        component={Link}
        to={routepath.DISH_RANK}
      >
        Dish Rank
      </Typography>
      <Button color="inherit" onClick={() => dispatch(logout())}>
        LOGOUT
      </Button>
    </div>
  );

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <div className={classes.title}>
            <Link to={routepath.HOME}>
              <IconButton size="small">
                <img
                  src={
                    "https://cdn.iconscout.com/icon/free/png-256/fast-food-1851561-1569286.png"
                  }
                  alt="Classroom"
                  className={classes.logo}
                />
              </IconButton>
            </Link>
          </div>
          {user ? authMenu : loginButton}
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default Header;
