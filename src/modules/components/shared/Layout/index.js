import React from "react";
import Header from "./Header";

const Layout = ({ children }) => {
  return (
    <div>
      <Header />
      <main style={{ marginTop: 64 }}>{children}</main>
    </div>
  );
};

export default Layout;
