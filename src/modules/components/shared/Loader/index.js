import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    flex: 1,
    height: "100vh",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
});

const Loader = ({ ...props }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CircularProgress {...props} />
    </div>
  );
};

export default Loader;
