import React from "react";
import { Button, CircularProgress } from "@material-ui/core";

const AppButton = ({ children, loading, disabled, onClick, ...props }) => {
  const onButtonClick = () => {
    if (!(loading || disabled)) {
      onClick();
    }
  };
  return (
    <Button {...props} onClick={onButtonClick}>
      {loading ? <CircularProgress size={24} color="inherit" /> : children}
    </Button>
  );
};

export default AppButton;
