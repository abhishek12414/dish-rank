import { createReducer } from "@reduxjs/toolkit";

import {
  setDishes,
  setDishesScore,
  setIsLoading,
  setUserSelectedDishes,
} from "../actions/dish";

const initialState = {
  dishes: null,
  isLoading: false,
  dishesScore: null,
  userSelectedDishes: [],
};

export const dishReducer = createReducer(initialState, {
  [setDishes]: (state, action) => {
    state.dishes = action.payload;
  },
  [setIsLoading]: (state) => {
    state.isLoading = !state.isLoading;
  },
  [setDishesScore]: (state, action) => {
    state.dishesScore = action.payload;
  },
  [setUserSelectedDishes]: (state, action) => {
    state.userSelectedDishes = action.payload;
  },
});

export default dishReducer;
