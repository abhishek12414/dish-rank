import { createReducer } from "@reduxjs/toolkit";
import { setUser, setIsLoading, setErrorMsg } from "actions/auth";

const initialState = {
  user: null,
  errorMsg: "",
  isLoading: true,
};

export const dishReducer = createReducer(initialState, {
  [setUser]: (state, action) => {
    state.user = action.payload;
  },
  [setIsLoading]: (state, action) => {
    state.isLoading = action.payload;
  },
  [setErrorMsg]: (state, action) => {
    state.errorMsg = action.payload;
  },
});

export default dishReducer;
