import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import dishReducer from "reducer/dish";
import userReducer from "reducer/user";
import { connectRouter, routerMiddleware } from "connected-react-router";
import { createBrowserHistory } from "history";

export const history = createBrowserHistory();

export const store = configureStore({
  reducer: {
    router: connectRouter(history),
    dish: dishReducer,
    user: userReducer,
  },
  middleware: [
    ...getDefaultMiddleware({ serializableCheck: false }),
    routerMiddleware(history),
  ],
  devTools: process.env.NODE_ENV !== "production",
});
