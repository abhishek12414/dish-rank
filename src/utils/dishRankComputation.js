const { getFromStorage, saveToStorage } = require("./storage");
const DISH_STORAGE_KEY = "dishRanks";
const RATING_SCORE = {
  1: 30,
  2: 20,
  3: 10,
};

const deleteCurrentUserChoices = (userId, records) => {
  Object.values(records).forEach((i) => {
    delete i[userId];
  });
};

const getUpdatedRecords = (userChoice, userId, records) => {
  const newValues = userChoice.reduce((acc, userChoice, index) => {
    return {
      ...acc,
      [userChoice]: {
        ...records[userChoice],
        [userId]: index + 1,
      },
    };
  }, {});

  return {
    ...records,
    ...newValues,
  };
};

export const updateDishRank = async (userChoice, userId) => {
  const dishRanks = await getFromStorage(DISH_STORAGE_KEY);
  let records = {};
  if (dishRanks) {
    records = JSON.parse(dishRanks);
    deleteCurrentUserChoices(userId, records);
  }

  const data = getUpdatedRecords(userChoice, userId, records);
  await saveToStorage(DISH_STORAGE_KEY, JSON.stringify(data));
};

// get dish rating and score

const computeRating = (data) => {
  return Object.values(data).reduce((acc, item) => {
    return {
      ...acc,
      [item]: acc[item] ? acc[item] + 1 : 1,
    };
  }, {});
};

const computeScore = (data) => {
  return Object.keys(data).reduce(
    (acc, item) => acc + data[item] * RATING_SCORE[item],
    0
  );
};

const getSortableDish = (data) =>
  data.sort((a, b) => (b?.score || 0) - (a?.score || 0));

export const getDishRatingAndScore = async (dishes) => {
  const dishRanks = await getFromStorage(DISH_STORAGE_KEY);
  if (dishRanks) {
    const result = JSON.parse(dishRanks);

    const dishScore = Object.keys(result).reduce((acc, data) => {
      const rating = computeRating(result[+data]);
      const score = computeScore(rating);
      return {
        ...acc,
        [data]: {
          rating,
          score,
        },
      };
    }, {});

    // add rating and score in original dish array
    dishes = dishes.reduce(
      (acc, item) => [...acc, { ...item, ...dishScore[item.id] }],
      []
    );

    return getSortableDish(dishes);
  }
  return dishes;
};

export const getUserSelectedDishes = async (userId) => {
  userId = userId.toString();
  const dishRanks = await getFromStorage(DISH_STORAGE_KEY);
  if (dishRanks) {
    const result = JSON.parse(dishRanks);
    const userChoice = Object.keys(result).reduce((acc, item) => {
      if (Object.keys(result[+item]).includes(userId)) {
        acc.push(+item);
      }
      return acc;
    }, []);
    return userChoice;
  }
  return [];
};
