export const saveToStorage = async (key, value) => {
  if (window?.localStorage) {
    return await window.localStorage.setItem(key, value);
  }
};

export const getFromStorage = async (key) => {
  if (window?.localStorage) {
    return await window.localStorage.getItem(key);
  }
};
